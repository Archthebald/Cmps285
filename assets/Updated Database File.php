<?php



//You should delete the tables that are in you database and run the following scripts one at a time so that your login
//functions will work for testing.


//To drop the all of the tables in developement, run the following in you testing database

/*

DROP TABLE Answers, Posts, Users;


 */


/*Users----------------------Copy Below


CREATE TABLE `Users` (
`ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(30) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `FIRST_NAME` varchar(30) DEFAULT NULL,
  `LAST_NAME` varchar(30) DEFAULT NULL,
  `ZIP` int(11) DEFAULT NULL,
  `LNG` float DEFAULT NULL,
  `LAT` float DEFAULT NULL,
  `LOCATION` point DEFAULT NULL,
  `ZOOM` int(11) DEFAULT NULL,
  `TECH` tinyint(1) NOT NULL DEFAULT '0',
  `COMPANY` varchar(30) DEFAULT NULL,
  `TITLE` varchar(30) DEFAULT NULL,
  `SALT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1





//Answers----------------------Copy Below

CREATE TABLE `Answers` (
`ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUTHOR` int(11) NOT NULL,
  `PARENT_POST` int(11) DEFAULT NULL,
  `CONTENT` longtext NOT NULL,
  `RESPONSE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `VISABLE` tinyint(1) NOT NULL DEFAULT '0',
  `PENNAME` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1






//Posts----------------------Copy Below

CREATE TABLE `Posts` (
`ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(30) NOT NULL,
  `CONTENT` longtext NOT NULL,
  `LNG` float NOT NULL,
  `LAT` float NOT NULL,
  `LOCATION` point DEFAULT NULL,
  `POSTED_BY` int(11) DEFAULT NULL,
  `SOLVED` tinyint(1) NOT NULL DEFAULT '0',
  `PRIVATE` tinyint(1) NOT NULL DEFAULT '0',
  `LOCAL_ONLY` tinyint(1) NOT NULL DEFAULT '0',
  `AUTHOR` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1


*/