<?php

$api = "AIzaSyDrMgU4eX0zkfmu26kcxCqi5svy_-mgCiQ";

?>

<!DOCTYPE html>

<html>

<head>


    <title>Tech Nøpe</title>
    <!-- Fontawesoome library>  -->
    <script src="https://use.fontawesome.com/a2ac9c9a20.js"></script>

    <!-- jquery library cdn -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>javascript/jqfloat/jqfloat.js" ></script>
    <!-- bootstrap  cdn and css links -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- css folder -->
    <?php echo link_tag('assets/css/mapcss.css'); ?>
    <?php echo link_tag('assets/images/technope.png'); ?>

    <title>Tech Nope</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>

    </style>

</head>

<body>

<!--wrapper class-->
<div id="wrapper" class="col-md-12 col-xs-12">

    <!--Start of banner div-->
    <div id="banner" class="col-md-12 col-xs-12">
        <!--navigation bar bootstrap-->
        <nav class="navbar navbar-default navbar-fixed-top techgradientcolor bottomgoldoutline">
            <div class="container-fluid">
                <div class="navbar-header">


                    <!--This needs to be refactored -->
                    <!--this button appears when the screen is phone width. Creates phone dropdown-->
                    <button type="button" class="navbar-toggle pull-right" data-toggle="collapse"
                            data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!--our banner-->
                    <a id="logo" class="navbar-brand" onclick="centermap();" href="#"></a>
                </div>
                <?php if (!$_SESSION['ID']) {
                    echo('
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a id="registerbtn" href="#" data-toggle="modal" data-target="#registermodal"
                                   class="pull-right"><span
                                        class="glyphicon glyphicon-user goldletters"> Sign Up</span></a></li>
                            <li><a id="loginbtn" class="pull-right" data-toggle="modal" data-target="#loginmodal"
                                   href="#"><span
                                        class="glyphicon glyphicon-log-in goldletters"> Login</span></a></li>
                        </ul>
                    </div>
');
                } else {
                    echo('

                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#"
                                   class="pull-right"><span onclick="ShowaccountSettings()"
                                        class="glyphicon glyphicon-user goldletters"> ' . $_SESSION["EMAIL"] . '</span></a></li>
                            <li><a class="pull-right" href="#">
                                <span onclick="Logout();" class="glyphicon glyphicon-random goldletters"> Logout</span></a></li>
                    </ul>
                    
                </div>

');
                }
                ?>
            </div>
        </nav>


    </div>
    <!--end of banner div-->

    <!-- todo add a hidden box with the post page behind the map, call for it when needed--------->


    <!-- Google maps div ------------------------------------------------------------------------->
    <div id="map" class="col-md-12 col-xs-12 container-fluid"></div>

    <!-- LOGIN FORM ------------------------------------------------------------------------------>
    <div id="loginmodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content blackgradientcolor">
                <div class="modal-header orangegradientcolor impact white bottomgoldoutline text-center">
                    <button type="button" class="close goldletters" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Login</h3>
                </div>
                <div class="modal-body">
                    <form id="login" class="" accept-charset="utf-8">
                        <div class=" form-group goldletters ">
                            <label for="email">Email address:</label>
                            <input type="email" class="form-control" name="email" id="email" required>
                        </div>
                        <div class="form-group goldletters">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" name="password" id="password" required>
                        </div>
                        <div class="checkbox goldletters">
                            <label><input type="checkbox"> Remember me</label>
                        </div>
                        <button id="loginprocess" type="submit" class="btn btn-default orangegradientcolor white impact">Submit</button>
                    </form>

                    <div id="LoginHelper" class="ErrorInvisible">Try again!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

<!-- todo  Registration form is here --!>
    <!-- REGISTRATION FORM -->
    <div id="registermodal" class="modal fade bs-example-modal-lg" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content blackgradientcolor">
                <div class="modal-header orangegradientcolor impact white bottomgoldoutline text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Sign Up</h3>
                </div>
                <div>
                    <form style="float: left;" id="register" class="" accept-charset="utf-8">

                        <div class="form-group goldletters">
                            <label for="firstName">First Name:</label>
                            <input type="text" class="form-control" name="CFN" id="First_Name" required>
                        </div>
                        <div class="form-group goldletters">
                            <label for="lastName">Last Name:</label>
                            <input type="text" class="form-control" name="CLN" id="Last_Name" required>
                        </div>

                        <div class="form-group goldletters">
                            <label for="email">Email address:</label>
                            <input type="email" class="form-control" name="CEM" id="email" required>
                        </div>

                        <div class="form-group goldletters">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" name="CPW" id="password" required>
                        </div>
                        <div class="form-group goldletters">
                            <label for="repeat password">Repeat Password:</label>
                            <input type="password" class="form-control" id="repeatPassword" required>
                        </div>
                        <div class="checkbox goldletters">
                            <!--todo  Make this required for registration -->
                            <label><input type="checkbox"> Please review our <a onclick="TermsandConditions()" href="#">terms
                                    and conditions</a></label>
                        </div>
                        <button type="submit" class="btn btn-default orangegradientcolor removeborders impact white">Submit</button>
                    </form>
                </div>
                <div class="modal-footer removetopborder">
                </div>
            </div>
        </div>
    </div>


    <div id="loginmodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header orangegradientcolor impact white bottomgoldoutline">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Login</h3>
                </div>
                <div class="modal-body">
                    <form id="login" class="" accept-charset="utf-8">
                        <div class=" form-group ">
                            <label for="email">Email address:</label>
                            <input type="email" class="form-control" name="email" id="email" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" name="password" id="password" required>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox"> Remember me</label>
                        </div>
                        <button id="loginprocess" type="submit" class="btn btn-default orangegradientcolor white impact">Submit</button>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>


    <!--
        This should be a list of all posts made by the current user
        when clicked, bring up the comment #CommentModal for that post ID
    -->

    <div id="ResponsesModal" class="modal fade bs-example-modal-lg" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content blackgradientcolor">
                <div class="modal-header  orangegradientcolor impact white text-center bottomgoldoutline">
                    <h3> Your Responses </h3>
                </div>
                <div id="PersonalResponses" class="row topmargin">

                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <!--
        This is the modal we should use for the on map click.  It shows the question and other responses,
        as well as a place to put a new message.
  -->
    <div id="CommentModal" class="modal fade bs-example-modal-lg " tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content techgradientcolor">
                <div id="comment_modal_header"
                     class="modal-header text-center orangegradientcolor bottomgoldoutline impact">
                    <p id="user_id"></p>
                    <p id="comment_title"></p>
                </div>
                <div>


                    <div class="container-fluid text-center">
                        <div class="row toppadding">
                            <div class="col-sm-3 well backgroundblack removeborderradius">
                                <div class="well orangegradientcolor ">
                                    <p class="orangegradientcolor" id="profile_pic" onclick="" href="#"></p>
                                    <img src="bird.jpg" class="img-circle" height="65" width="65" alt="Avatar">
                                </div>
                            </div>
                            <div class="col-sm-9 removeleftrightborders removeborderradius ">
                                <div class="panel panel-default text-left removeleftrightborders">
                                    <div id="poster_content"
                                         class="panel-body text-center backgroundblack bottomgoldoutline impact">
                                        <p contenteditable="false"></p>
                                    </div>
                                </div>
                                <button id="Respond" type="button"
                                        class="btn btn-default btn-sm orangegradientcolor impact removeborders">
                                    <span>Post Comment</span>
                                </button>
                            </div>
                            <!--Append here-->
                            <div id="comment_area" class="col-sm-12">

                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <footer class="container-fluid text-center">
                <p>

                </p>
            </footer>

            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!-- FAQ Modal -->
<div id="FAQModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header orangegradientcolor impact white text-center bottomgoldoutline">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">DON'T PANIC!</h4>
            </div>
            <div class="modal-body blackgradientcolor bottomgoldoutline">
                <p class="goldletters">We have your towel <b>right</b> here.</p>
                <p>
                <ul>
                    <li><a href="http://stackoverflow.com/">How do code??</a></li>
                    <li><a href="https://lmgtfy.com/?q=Wont+power+up">Wont power up?</a></li>
                    <li><a href="https://lmgtfy.com/?q=Mouse+problems">Mouse problems?</a></li>
                    <li><a href="https://lmgtfy.com/?q=google">Can't get right?</a></li>
                    <li><a href="https://lmgtfy.com/?q=google">Can't get Wrong?</a></li>
                    <li><a href="https://lmgtfy.com/?q=google">What are you even doing here?</a></li>
                    <li><a href="https://lmgtfy.com/?q=google">Want a job?</a></li>
                    <li><a href="https://lmgtfy.com/?q=google">Stop looking at it!</a></li>
                    <li><a href="https://lmgtfy.com/?q=google">It's just a list!!!</a></li>
                    <li><a href="https://lmgtfy.com/?q=google">Give us an A?</a></li>
                    <li><a href="https://lmgtfy.com/?q=google">How about a job?</a></li>
                </ul>
                </p>
            </div>
            <div class="modal-footer blackgradientcolor removetopborder">
                <button type="button" class="btn btn-default orangegradientcolor impact white removeborders" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--

-->
<!--
    This is where a user creates a question, They should have the ability to set the location of the question
    until we can find a better option for it.  At worst, use the browser function for current location.
-->

<div id="CreatePostModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header orangegradientcolor text-center bottomgoldoutline">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title impact">Problems?</h4>
            </div>
            <div class="modal-body blackgradientcolor bottomgoldoutline">
                <div>
                    <div class="container-fluid blackgradientcolor">
                        <form class="form-inline textwhite">
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    <small>Lng</small>
                                </label>
                                <input type="text" class="form-control" name="LNG" id="LNG" placeholder="where">
                            </div>
                            <div class="form-group ">
                                <label for="exampleInputEmail1">
                                    <small>Lat</small>
                                </label>
                                <input type="text" class="form-control" name="LAT" id="LAT" placeholder="you at?">
                            </div>
                        </form>
                        <form>
                            <div class="form-group textwhite">
                                <label>Title</label>
                                <input type="text" class="form-control" name="Title" id="Title" placeholder="Title">
                            </div>
                            <div class="form-group textwhite">
                                <label for="PostContent">Content</label>
                                <textarea class="form-control" name="Content" id="PostContent" rows="4"></textarea>
                            </div>

                            <button type="submit" class="orangegradientcolor removeborders impact white" onclick="create_Post()">Create Post
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer blackgradientcolor removetopborder">
                <button type="button" class="btn btn-default orangegradientcolor impact white removeborders" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<div id="PostCommentModal" class="modal fade bs-example-modal-lg" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3>Show them what you got!</h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="comment">Response:</label>
                    <textarea class="form-control" rows="5" id="CommentContent"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="" id="SubmitComment" class="btn btn-default orangegradientcolor impact white"
                        data-dismiss="modal">Submit
                </button>
            </div>
        </div>
    </div>
</div>

<div id="AccountModal" class="modal fade bs-example-modal-lg" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content blackgradientcolor">
            <div class="modal-header orangegradientcolor bottomgoldoutline text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title impact white">Change your identity here!</h3>
            </div>
            <div class="">
                <form id="SettingChange" class="largestleftpercentage" accept-charset="utf-8">

                    <div class="form-group center-text">
                        <label for="firstName"class="goldletters">First Name:</label>
                        <input type="text" class="form-control goldletters" id="First_Namef" placeholder="First Name">
                    </div>
                    <div class="form-group">
                        <label for="lastName" class="goldletters">Last Name:</label>
                        <input type="text" class="form-control goldletters" id="Last_Namef" placeholder="Last Name">
                    </div>

                    <div class="form-group">
                        <label for="email" class="goldletters">Email address:</label>
                        <input type="email" class="form-control" id="EMAILf" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <label for="ZIP" class="goldletters">ZIP:</label>
                        <input type="text" class="form-control" id="ZIPf" placeholder="ZIP Code">
                    </div>
                    <div class="text-centered">
                    <button type="submit" onclick="submitaccountsettings()" class="btn btn-default orangegradientcolor removeborders impact white">Submit</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer topgoldoutline">
            </div>
        </div>
    </div>
</div>

<div id="PostsModal" class="modal fade bs-example-modal-lg" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header orangegradientcolor impact white text-center bottomgoldoutline">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3>All of your Posts</h3>
            </div>
            <div id="PersonalPosts" class="modal-body blackgradientcolor row removeleftrightmargins">
                <p>Load all things</p>
            </div>
            <div class="modal-footer blackgradientcolor">
            </div>
        </div>
    </div>
</div>

<div id="ContactModal" class="modal fade bs-example-modal-lg" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header orangegradientcolor impact white text-center bottomgoldoutline">
                <button type="button" class="close orangegradientcolor" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3>You Rang?</h3>
            </div>
            <div class="modal-body blackgradientcolor bottomgoldoutline">
                <form class="form-horizontal">
                    <fieldset>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-4 control-label goldletters" for="RESPONSE_DROP">Select an area</label>
                            <div class="col-md-4">
                                <select id="RESPONSE_DROP" name="RESPONSE_DROP" class="form-control">
                                    <option value="1">*select one*</option>
                                    <option value="2">Report site issue</option>
                                    <option value="3">Report user</option>
                                    <option value="4">Report post</option>
                                    <option value="5">Lonely</option>
                                    <option value="6">Give use a thanks</option>
                                    <option value="7">Jobs?</option>
                                </select>
                            </div>
                        </div>

                        <!-- Textarea -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="RESPONSE_FIELD"></label>
                            <div class="col-md-4">
                                <textarea class="form-control" id="RESPONSE_FIELD" name="RESPONSE_FIELD"></textarea>
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="RESPONSE_BUTTON"></label>
                            <div class="col-md-4 text-right">
                                <button onclick="closecommentmodal()" id="RESPONSE_BUTTON" name="RESPONSE_BUTTON"
                                        class="btn btn-info orangegradientcolor impact white removeborders">Submit
                                </button>
                            </div>
                        </div>

                    </fieldset>
                </form>

            </div>
            <div class="modal-footer blackgradientcolor removetopborder">
            </div>
        </div>
    </div>
</div>


<?php if ($_SESSION['ID']) {
    echo('
    <nav class="main-menu">
        <ul>
            <li>
                <a onclick="ShowPostModal()" href="#">
                    <i class="fa fa-map-marker fa-1x"></i>
                    <span class="nav-text"> New Post </span>
                </a>
            </li>
            <li class="has-subnav">
                <a onclick="showPosts()" href="#">
                    <i class="fa fa-laptop fa-2x"></i>
                    <span  class="nav-text"> My Posts </span>
                </a>
            </li>
            <li class="has-subnav">
                <a onclick="ShowResponses()" href="#">
                    <i class="fa fa-list fa-2x"></i>
                    <span class="nav-text"> Responses </span>
                </a>
            </li>
            <li>
                <a onclick="ShowaccountSettings()" href="#">
                    <i class="fa fa-user fa-2x"></i>
                    <span class="nav-text"> Account </span>
                </a>
            </li>
            <li class="has-subnav">
                <a onclick="ShowContact()" href="#">
                    <i class="fa fa-envelope fa-2x pull-bottom"></i>
                    <span class="nav-text"> Contact </span>
                </a>
            </li>
        </ul>
        
        <ul class="logout">
            <li class="has-subnav">
                <a onclick="FAQ()" href="#">
                    <i class="fa fa-question fa-2x pull-bottom"></i>
                    <span class="nav-text"> FAQ </span>
                </a>
            </li>
        </ul>
</ul>
    </nav>

');
};
?>

</div>
</body>


<script>
    var map;

    // Map initialization
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            //If logged in sets location to user set center.
            //resets to school if on null island

            <?php if (!$_SESSION['ID']) {
            echo('center: {lat: 30.514751, lng: -90.466554},');
        } else
            if ($_SESSION['LNG'] == $_SESSION['LAT']) {
                echo('center: {lat: 30.514751, lng: -90.466554},');
            } else {
                echo('center: {lat: ' . $_SESSION['LNG'] . ', lng:' . $_SESSION['LAT'] . '},');
            }
            ?>
            zoom: 14,
            maxZoom: 18,
            minZoom: 5,
            styles: [
                {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                {
                    featureType: 'administrative.locality',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#f2f0ed'}]
                },
                {
                    featureType: 'poi',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'geometry',
                    stylers: [{color: '#263c3f'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#ffffff'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry',
                    stylers: [{color: '#634c12'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#212a37'}]
                },
                {
                    featureType: 'road',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#ffffff'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry',
                    stylers: [{color: '#f9bb1d'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#1f2835'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#ffffff'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'geometry',
                    stylers: [{color: '#2f3948'}]
                },
                {
                    featureType: 'transit.station',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'water',
                    elementType: 'geometry',
                    stylers: [{color: '#212428'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#515c6d'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.stroke',
                    stylers: [{color: '#17263c'}]
                }, // remove munisipality texts
                {
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#212121"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#757575"
                        },
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative.country",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative.locality",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "administrative.neighborhood",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#181818"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#1b1b1b"
                        }
                    ]
                }
            ],
            disableDefaultUI: true
        });

        var infoWindow = new google.maps.InfoWindow({
            pixelOffset: new google.maps.Size(0, 0),
            maxWidth: 250
        });

        google.maps.event.addListener(map, 'click', function() {
            infoWindow.close();
        });
        do {
            mark()
        }
        while (false);

        setInterval(mark, 4000);

        function mark() {

            downloadUrl("<?php echo base_url();?>" + "index.php/Posts/getAllPostsXML", function (data) {

                var xml = data.responseXML;

                var markers = xml.documentElement.getElementsByTagName("element");

                for (var i = 0; i < markers.length; i++) {

                    var ID = markers[i].getElementsByTagName("ID")[0].childNodes[0].nodeValue;
                    var title = markers[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue;
                    var content = markers[i].getElementsByTagName("CONTENT")[0].childNodes[0].nodeValue;
                    //var type = markers[i].getAttribute("type");
                    var point = new google.maps.LatLng(
                        parseFloat(markers[i].getElementsByTagName("LNG")[0].childNodes[0].nodeValue),
                        parseFloat(markers[i].getElementsByTagName("LAT")[0].childNodes[0].nodeValue)
                    );


// Define your local and call variables here!----------------------------------
// This is an anonymous inner class, no variables are accessible out of here---
// I feel like that guy coding the universe right now, no lie.

                    var html =
                        '<div id="iw-container">' +
                        '<div class="iw-title techgradientcolor">' + title + '</div>' +
                        '<div class="iw-content">' +
                        '<p>' + content + '</p>' +
                        '<div class="iw-subTitle invisible">  STOP SELECTING RANDOM THINGS!</div>' +
                        '<button id="PostShow" type="button" class="btn btn-primary btn-sm pull-right orangegradientcolor" ' +
                        ' onclick="<?php if ($_SESSION['ID']) {
                            echo("render_posts(' + ID + '); renderComments(' + ID + ');");
                        } else {
                            echo("ShowLogin();");
                        } ?>" >Respond</button>' +
                        '</div>' +
                        '<div class="iw-bottom-gradient purplegradientcolor"></div>' +
                        '</div>';


                    var icon = '<?php echo base_url("/assets/images/pat_marker_final.png") ?>'; //customIcons[type] || {};


                    var marker = new google.maps.Marker({
                        map: map,
                        position: point,
                        icon: icon
                    });


                    bindInfoWindow(marker, map, infoWindow, html);
                    google.maps.event.addListener(infoWindow, 'domready', function () {

                        // Reference to the DIV that wraps the bottom of infowindow
                        var iwOuter = $('.gm-style-iw');

                        /* Since this div is in a position prior to .gm-div style-iw.
                         * We use jQuery and create a iwBackground variable,
                         * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
                         */
                        var iwBackground = iwOuter.prev();

                        // Removes background shadow DIV
                        iwBackground.children(':nth-child(2)').css({'display': 'none'});

                        // Removes white background DIV
                        iwBackground.children(':nth-child(4)').css({'display': 'none'});

                        // Moves the infowindow 115px to the right.
                        iwOuter.parent().parent().css({left: '60px'});

                        // Moves the shadow of the arrow 76px to the left margin.
                        iwBackground.children(':nth-child(1)').attr('style', function (i, s) {
                            return s + 'left: 76px !important;'
                        });

                        // Moves the arrow 76px to the left margin.
                        iwBackground.children(':nth-child(3)').attr('style', function (i, s) {
                            return s + 'left: 76px !important;'
                        });

                        // Changes the desired tail shadow color.
                        iwBackground.children(':nth-child(3)').find('div').children().css({
                            'box-shadow': 'rgb(137,137,186) 0px 1px 6px',
                            'z-index': '1'
                        });

                        // Reference to the div that groups the close button elements.
                        var iwCloseBtn = iwOuter.next();

                        // Apply the desired effect to the close button
                        iwCloseBtn.css({
                            opacity: '1',
                            right: '38px',
                            top: '3px',
                            border: '7px solid rgb(137,137,186)',
                            'border-radius': '13px',
                            'box-shadow': '0 0 5px #3990B9'
                        });

                        // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
                        if ($('.iw-content').height() < 140) {
                            $('.iw-bottom-gradient').css({display: 'none'});
                        }

                        // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
                        iwCloseBtn.mouseout(function () {
                            $(this).css({opacity: '1'});
                        });
                    });
                    google.maps.event.addListener(map, "rightclick", function (event) {
                        showContextMenu(event.latLng);
                    });
                }


                if (SessionData['id']) {

                    //----Right click events--
                    //Globals to input coordinates for creating the marker
                    var x;
                    var y;

                    function getCanvasXY(caurrentLatLng) {
                        var scale = Math.pow(2, map.getZoom());
                        var nw = new google.maps.LatLng(
                            map.getBounds().getNorthEast().lat(),
                            map.getBounds().getSouthWest().lng()
                        );
                        var worldCoordinateNW = map.getProjection().fromLatLngToPoint(nw);
                        var worldCoordinate = map.getProjection().fromLatLngToPoint(caurrentLatLng);

                        var caurrentLatLngOffset = new google.maps.Point(
                            Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale),
                            Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale)
                        );
                        return caurrentLatLngOffset;
                    }

                    function setMenuXY(caurrentLatLng) {
                        console.log("setmenu working!");
                        var mapWidth = $('#map').width();
                        var mapHeight = $('#map').height();
                        var menuWidth = $('.contextmenu').width();
                        var menuHeight = $('.contextmenu').height();
                        var clickedPosition = getCanvasXY(caurrentLatLng);
                        x = clickedPosition.x;
                        y = clickedPosition.y;
                        if ((mapWidth - x ) < menuWidth)
                            x = x - menuWidth;
                        if ((mapHeight - y ) < menuHeight)
                            y = y - menuHeight;

                        $('.contextmenu').css('left', x);
                        $('.contextmenu').css('top', y);
                    }

                    function showContextMenu(caurrentLatLng) {
                        console.log("showcontextmenu working!");
                        var projection;
                        var contextmenuDir;
                        var lat = caurrentLatLng.lat();
                        var lng = caurrentLatLng.lng();
                        console.log(lat);
                        projection = map.getProjection();
                        $('.contextmenu').remove();
                        contextmenuDir = document.createElement("div");
                        contextmenuDir.className = 'contextmenu';
                        contextmenuDir.innerHTML = "<a id='menu1' onclick = ShowPostModal(" + lng + "," + lat + ") href='#''><div class='context impact goldletters'><span class='glyphicon glyphicon-map-marker'></span>Create Post<\/div>" +
                            "<\/a><a id='menu2' onclick = showPosts()><div class='context impact goldletters'><span class='glyphicon glyphicon glyphicon-question-sign'></span>Posts<\/div>" +
                            "<\/a><a id='menu2' onclick = ShowResponses()><div class='context impact goldletters'><span class='glyphicon glyphicon-info-sign'></span>Responses<\/div><\/a>";
                        $(map.getDiv()).append(contextmenuDir);

                        setMenuXY(caurrentLatLng);

                        contextmenuDir.style.visibility = "visible";
                    }

                    //HIDES MENU WHEN NOT CLICKED ON

                    $(document).mouseup(function (e) {
                        var container = $(".contextmenu");
                        if (!container.is(e.target) // if the target of the click isn't the container...
                            && container.has(e.target).length === 0) // ... nor a descendant of the container
                        {
                            container.hide();
                        }
                    });


                }


            });
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------CONTEXT HERE



    }


    function bindInfoWindow(marker, map, infoWindow, html) {
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        });
    }

    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                request.onreadystatechange = doNothing;
                callback(request, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }


    function PopulatePostData(ID) {
        render_posts(ID);
    }


    function doNothing() {
    }


    $(document).ready(function () {

//Start the two forms hidden to be called later
        $("#map").hide();

//FadeIn Map
        $("#map").fadeIn(1734);

// Login Form ajax-----------------------
        $("#login").submit(function () {

            ////console.log("clicked!");
            //console.log('<?php echo $_SESSION['EMAIL'] ?>');
            var loginform = {
                'password': btoa($('input[name=password]').val()),
                'email': $('input[name=email]').val()
            };

            if (loginform['email'] == "Nick@night.edu") {
                alert("oooOOOOOoooo");
            }


//Login Form ajax----->
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url(); ?>" + "index.php/UserLogin/logincheck",
                dataType: "text",
                data: loginform,
                success: function (rtn) {
                    if (rtn == 0) {
                        window.location.reload(false);
                    }
                    if (rtn == 1) {
                        console.log("login incorrect");
                        document.getElementById("LoginHelper").className = "alert alert alert-warning";
                        // todo inform them they incorrectly logged in
                    }
                    else console.log();

                },
                error: function (rtn) {
                    console.log(rtn["responseText"]);
                }
            });
            event.preventDefault();
        });

// Register Form ajax-------------------
        $("#register").submit(function () {

            //console.log(HashPass($('input[name=CPW]').val()));

            var registerForm = {
                'firstname': $('input[name=CFN]').val(),
                'lastname': $('input[name=CLN]').val(),
                'password': btoa($('input[name=CPW]').val()),
                'email': $('input[name=CEM]').val()
            };

            $.ajax({
                type: 'POST',
                url: "<?php echo base_url(); ?>" + "index.php/Process/validate",
                dataType: "text",
                data: registerForm,
                success: function (rtn) {
                    if (rtn == 0) {
                        alert("You are now a member and logged in!!");
                        window.location.reload(false);
                    }
                    if (rtn == 1) {
                        $('#registermodal').modal('hide');

                        alert("You already have an account, LOGIN already!!");

                    }
                    else {
                        console.log(rtn);
                    }
                },
                error: function (rtn) {
                    console.log(rtn["responseText"]);
                }
            });
            event.preventDefault();
        });
    });

    // Logout Function----------------------
    function Logout() {

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "index.php/UserLogin/UserLogout",
            success: function (rtn) {
                window.location.reload(false);

            },
            error: function (rtn) {
                console.log(rtn["responseText"]);
            }

        });

    }


    function render_posts(id) {


        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "index.php/Posts/getPostByID",
            dataType: "json",
            data: {
                'id': id
            },
            success: function (rtn) {
                document.getElementById("profile_pic").innerHTML = rtn.AUTHOR;
                document.getElementById("comment_modal_header").innerHTML = '<a ></a>' + rtn.title;
                document.getElementById("poster_content").innerHTML = '<a class="glyphicon glyphicon-exclamation-sign purple"></a> ' + rtn.content;
                document.getElementById("Respond").setAttribute("Onclick", "AuthorComment(" + id + ")");
                //console.log(rtn);
            },
            error: function (rtn) {
                console.log(rtn["responseText"]);
            }

        });

        $('#CommentModal').modal('toggle');

    }

    // Reset google maps to default center -------
    function centermap() {
        //get todo this to recenter to current location
        window.location.reload(false);

    }

    // like Click the sidebar button for create question
    // Modal for creating and posting a question
    function create_Post() {
        var postdata = [$("#Title").val(), $("#PostContent").val(), $('input[id=LNG]').val(), $('input[id=LAT]').val(), "false", "false", SessionData['first_name']];

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "index.php/Posts/CreatePost",
            dataType: "text",
            data: {
                '1': postdata[0],
                '2': postdata[1],
                '3': postdata[2],
                '4': postdata[3],
                '5': SessionData['id'],
                '6': postdata[4],
                '7': postdata[5],
                '8': postdata[6]
            },
            success: function (rtn) {
                //console.log("success");
                //window.location.reload(false);
                $("#CreatePostModal").modal("toggle");
                $("#Title").val("");
                $("#PostContent").val("");
                Sanatize("Title");
                Sanatize("PostContent");
                Document.preventDefault();

            },
            error: function (rtn) {
                console.log(rtn + "err");
            }
        });

        event.preventDefault();
    }


    // Show all questions the current user has asked.
    function showPosts() {

        var id = SessionData['id'];


        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "index.php/Posts/getPostByUser",
            dataType: "json",
            data: {
                'UID': id
            },
            success: function (rtn) {
                //console.log(rtn);
                $("#PersonalPosts").html("");
                if (rtn == 0) {
                    $("#PersonalPosts").append('<row>  You have no posts!!! </row>');
                }
                else {
                    for (var i = 0; i < rtn.length; i++) {
                       // $("#Respond").attr("onclick", "AuthorComment(" + id + ")");
                        console.log(rtn[i]);
                        var posts = '<div class="col-sm-3">' +
                            '<div class="well purplegradientcolor removeborders impact text-center">' +
                            '<img src="bird.jpg" class="img-circle" height="55" width="55" alt="Avatar">' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-sm-9">' +
                            '<div class="well blackgradientcolor removeborderradius  removetoppadding removeleftborders removetopborder rightgoldoutline bottomgoldoutline">' +
                            '<p id="comment_id">' +
                            '<div class="well removeleftborders removeborderradius  removetoppadding backgroundblack impact removetopborder rightgoldoutline bottomgoldoutline">' +
                            '<h2 onclick="these(' + rtn[i].ID + ')" class="posttitle textunderline removetopmargin text-center">' + rtn[i].TITLE +
                            '</h2>' +
                            '<p class="helvetica">' + rtn[i].CONTENT +
                            '</p>' +
                            '</div>' +
                            '<div class="text-right">' +
                            '<button type="button" class="btn btn-default btn-sm orangegradientcolor impact rightmargin removeborders" onclick="Allofthese(' + rtn[i].ID + ');" href="#">Go to Post  ' +
                            '<button onclick="RemovePost(' + rtn[i].ID + ')" type="button" class="btn btn-default btn-sm orangegradientcolor impact removeborders"> Remove post  ' +
                            '</button>';
                            '</div>' +
                            '</div>';


                        $("#PersonalPosts").append('<row>' + posts + '</div>');
                    }
                }
            },
            error: function (rtn) {
                consol.log("fail");
                console.log(rtn);
            }

        });


        $('#PostsModal').modal('toggle');

        //console.log('posts activated');

    }
    function Allofthese(id) {
        console.log(id);
        HidePostsbyuser();
        renderComments(id);
        render_posts(id);
    }

    /*  Show the accounts page with ability to change
     Email-address
     password
     names
     zip-code
     */
    function renderComments(id) {

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "index.php/Responses/GetResponsesByParent",
            dataType: "json",
            data: {
                'PID': id
            },
            success: function (rtn) {
                //console.log(rtn);


                $("#comment_area").html("");

                if (rtn == 0) {

                    //$("#comment_area").append("You have no comments!!!");
                }
                else {
                    for (var i = 0; i < rtn.length; i++) {
                        console.log(rtn[i]);
                        var comment = '<div class="row">' +
                            '<div class="col-sm-3">' +
                            '<div class="well blackgradientcolor removebordradius impact">' +
                            '' +
                            '<img src="bird.jpg" class="img-circle" height="55" width="55" alt="Avatar">' +
                            '</div>' +
                            '<div class="backgroundblack impact">' + rtn[i].PENNAME + '</div>' +
                            '</div>' +
                            '<div class="col-sm-9">' +
                            '<div class="well blackgradientcolor removeborderradius removeleftrightborders bottomgoldoutline">' +
                            '<p id="comment_id">' +
                            '<div class="well removeleftrightborders removeborderradius backgroundblack impact">' +
                            '<p>' + rtn[i].CONTENT +
                            '</p>' +
                            '</div>' +
                            '<div class="pull-right">' +
                            '<button type="button" class="btn btn-default btn-sm">' +
                            '<span class="glyphicon glyphicon-thumbs-up"></span>' +
                            '</button>' +
                            '<button type="button" class="btn btn-default btn-sm">' +
                            '<span class="glyphicon glyphicon-thumbs-down"></span>' +
                            '</button>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                        $("#comment_area").append(comment);
                    }
                }
            },
            error: function (rtn) {
                console.log(rtn);
            }

        });

    }

    function Comment(comment) {

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "index.php/Responses/PostComent",
            dataType: "text",
            data: {
                '1': comment[0],
                '2': comment[1],
                '3': comment[2],
                '4': comment[3]
            },
            success: function (rtn) {
                //location.reload(true);
            },
            error: function (rtn) {

                console.log("DONT MIND THIS ERRRR!");
            }

        });

    }
    function Sanatize(ID) {
        $('#CommentContent').value = "";

        document.getElementById(ID).innerHTML = "";
    }
    function AuthorComment(id) {
        var ParentPost = id;

        $('#CommentModal').modal('toggle');
        Sanatize("CommentContent");
        $('#PostCommentModal').modal('show');


        document.getElementById("SubmitComment").getAttributeNode("onclick").value = "idkwtfbbq(" + ParentPost + ")";


    }
    function idkwtfbbq(ParentPost) {
        var content = [SessionData['id'], ParentPost, $("#CommentContent").val(), SessionData['first_name']];

        console.log(content);

        Comment(content);


    }


    //  Alert that we respond to nothing!@!!@2#!@!
    function TermsandConditions() {
        alert("WE ARE RESPONIBLE FOR NOTHING!!!!!  " +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "Good Day Sir!");

    }

    function ShowLogin() {
        $('#loginmodal').modal('show');
    }

    function ShowPostModal(lng,lat) {

        $('input[id=LNG]').val(lat);
        $('input[id=LAT]').val(lng);
        $('#CreatePostModal').modal('toggle');

    }

    function HidePostsbyuser(){
         $('#PostsModal').modal('hide');
        $('#ResponsesModal').modal('hide');

    }


    function FAQ() {

        $('#FAQModal').modal('show');
    }


    function ShowContact() {

        $('#ContactModal').modal('show');

    }

    function closecommentmodal() {

        $('#ContactModal').modal('hide');

        $("#RESPONSE_DROP").val(1);
        $("#RESPONSE_FIELD").val("");
        event.preventDefault();
    }


    //<----------------------------------------------------------------------------------------------------------------------------->  RIGHT HERE PATRICK!!!!!
    function ShowResponses() {


        Sanatize("PersonalResponses");

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "index.php/Responses/GetParentByAuthorID",
            dataType: "json",
            data: {
                'UID': SessionData['id'],
            },

            success: function (rtn) {

                console.log(rtn);
                if (rtn == 0) {

                }
                for (var i = 0; i < rtn.length; i++) {

                    // $("#Respond").attr("onclick", "AuthorComment(" + id + ")");
                    console.log(rtn[i]);
                    var posts = '<div class="col-sm-3">' +
                        '<div class="well removeborders purplegradientcolor impact text-center">' +                   
                        '<img src="bird.jpg" class="img-circle" height="55" width="55" alt="Avatar">' +
                        rtn[i].AUTHOR +
                        '</div>' +
                        '</div>' +
                        '<div class="col-sm-9">' +
                        '<div class="well blackgradientcolor removeborderradius removetoppadding removeleftborders removetopborder rightgoldoutline bottomgoldoutline">' +
                        '<p id="comment_id">' +
                        '<div class="well removeleftborders removeborderradius removetopborder removetoppadding rightgoldoutline bottomgoldoutline backgroundblack impact">' +
                        '<h2 onclick="Allofthese(' + rtn[i].ID + ')" class="posttitle text-center textunderline">' + rtn[i].TITLE +
                        '</h2>' +
                        '<p class="helvetica">' + rtn[i].CONTENT +
                        '</p>' +
                        '</div>' +
                        '<div class="text-right">' +
                        '<button type="button" class="btn btn-default btn-sm orangegradientcolor impact white removeborders" onclick="Allofthese(' + rtn[i].ID + ');" href="#">Go to Post  ' +
                        '</div>' +
                        '</div>';


                    $("#PersonalResponses").append('<row>' + posts + '</row>');
                }
            },
            error: function (rtn) {
                console.log(rtn);
            }

        });

        $('#ResponsesModal').modal('show');

    }
    function ShowaccountSettings() {


        $('#AccountModal').modal('show');

        var CurEmail = SessionData['email'];
        var CurFName = SessionData['first_name'];
        var CurLName = SessionData['last_name'];
        var CurZIP = SessionData['zip'];


        $("#EMAILf").attr("placeholder", CurEmail);
        $("#First_Namef").attr("placeholder", CurFName);
        $("#Last_Namef").attr("placeholder", CurLName);
        $("#ZIPf").attr("placeholder", CurZIP);


    }

    function submitaccountsettings() {

        var zip = $("#ZIPf").val();

        $.ajax({
            url: "http://maps.googleapis.com/maps/api/geocode/json?address=" + zip + "&sensor=false"
            , success: function (data) {


                var lat = data.results[0].geometry.location.lat;
                var lng = data.results[0].geometry.location.lng;

                SubmitaccountCallback(lat, lng);

            },
            error: function (data) {
                console.log("Location Error Occured: this has been logged:" + Math.random());
                SubmitaccountCallback(null, null);
            }
        });

    }

    function SubmitaccountCallback(lng, lat) {



        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "index.php/Process/SubmitAccountChanges",
            dataType: "text",
            data: {
                'id': SessionData['id'],
                'email': $("#EMAILf").val(),
                'first_name': $("#First_Namef").val(),
                'last_name': $("#Last_Namef").val(),
                'zip': $("#ZIP").val(),
                'lat': lat,
                'lng': lng
            },
            success: function (rtn) {
                location.reload(true);
            },
            error: function (rtn) {
                console.log(0);
            }

        });


    }

    function RemovePost(pid) {

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>" + "index.php/Posts/RemovePostByID",
            dataType: "text",
            data: {
                'pid': pid,
            },
            success: function (rtn) {
                location.reload(false);

                showPosts(SessionData['id']);
            },
            error: function (rtn) {
            }

        });

    }

    var SessionData = {
        id: "<?php echo $_SESSION['ID']?>",
        email: "<?php echo $_SESSION['EMAIL']?>",
        first_name: "<?php echo $_SESSION['FIRST_NAME']?>",
        last_name: "<?php echo $_SESSION['LAST_NAME']?>",
        zip: "<?php echo $_SESSION['ZIP']?>"
    }
</script>


<!--DONT USE THIS ONE!!!!!!-->
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo($api) ?>&callback=initMap"
        async defer></script>
</body>
</html>






























