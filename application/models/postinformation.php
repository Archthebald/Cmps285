<?php


class postinformation extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function allPostInfo()
    {
        $query = $this->db->query("SELECT ID, TITLE, CONTENT, LAT,LNG FROM Posts WHERE SOLVED != 1;");

        return $query;
    }

    function CreatePost($Post)
    {
        $Insert = array(
            'TITLE' => $Post[1],
            'CONTENT' => $Post[2],
            'LNG' => $Post[3],
            'LAT' => $Post[4],
            'POSTED_BY' => $Post[5],
            'SOLVED' => $Post[6],
            'LOCAL_ONLY' => $Post[7],
            'AUTHOR' => $Post[8]
        );
        echo $Insert;

        $this->db->insert('Posts', $Insert);

    }

    function Postbyid($ID)
    {

        $query = $this->db->query("SELECT ID, TITLE, CONTENT, LAT,LNG, AUTHOR FROM Posts WHERE ID = '" . $ID . "';");

        $row = $query->row_array();


        if (isset($row)) {
            $return['ID'] = $row['ID'];
            $return['content'] = $row['CONTENT'];
            $return['title'] = $row['TITLE'];
            $return['posted_by'] = $row['POSTED_BY'];
            $return['AUTHOR'] = $row['AUTHOR'];
        }

        return $return;

    }

    function GetPostByUser($UID)
    {


        $query = $this->db->query("SELECT * FROM Posts WHERE POSTED_BY = '" . $UID . "';");

        return $query->result_object();
    }

    function UpdatePostByID($ID)
    {

        $update = $this->db->query("UPDATE LOW_PRIORITY IGNORE Posts SET TITLE='" . $ID[0] . "', CONTENT='" . $ID[1] . "', LOCAL_ONLY='" . $ID[2] . "', LAT='" . $ID[3] . "', LNG='" . $ID[4] . "', PRIVATE='" . $ID[5] . "'WHERE ID='" . $ID[6] . "'LIMIT 1;");

        if ($update === false) {
            return 1;
        }
        return 0;
    }

    function MakePostIDSolved($ID)
    {
        $update = $this->db->query("UPDATE LOW_PRIORITY IGNORE Posts SET SOLVED = TRUE WHERE ID='" . $ID . "';");
        if ($update === false) {
            return 1;
        }
        return 0;
    }

    function RemovePostByID($PID)
    {
        $remove = $this->db->query("DELETE LOW_PRIORITY Posts FROM Posts WHERE ID ='" . $PID . "';");
        if ($remove === false) {
            return 1;
        }
        return 0;
    }


}