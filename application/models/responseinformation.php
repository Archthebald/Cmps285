<?php

class responseinformation extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function GetParentByAuthorModel($UID)
    {

        $query = $this->db->query("SELECT DISTINCT Posts.ID, Posts.TITLE, Posts.CONTENT FROM Posts LEFT JOIN Answers ON Posts.ID = '" . $UID . "';");
        /*
                $row = $query->row_array();

                if (isset($row)) {
                    $return['ID'] = $row['ID'];
                    $return['content'] = $row['CONTENT'];
                    $return['title'] = $row['TITLE'];
                }
        */

        return $query->result_object();
    }

    function GetResponsesByParentModel($PID)
    {
        $query = $this->db->query("SELECT * FROM Answers WHERE PARENT_POST = " . $PID . ";");

        if (!$query) {
            return null;
        }
        if ($query === null) {
            return null;
        }

        $parsed = $query->result_array();

        return $parsed;
    }

    function CreateResopnseByParent($data)
    {


        $response = $this->db->insert('Answers', $data);

        //$this->db - query("INSERT INTO Answers(AUTHOR, PARENT_POST, CONTENT, RESPONSE_TIME, VISABLE) VALUES ('" . $data[0] . "','" . $data[1] . "','" . $data[2] . "','" . $data[3] . "','" . $data[4] . "');");

        if ($response === false) {

            return 1;
        }

        return $response;
    }

    function RemoveResponseByID($ID)
    {
        $response = $this->db->query("DELETE Answers FROM Answers WHERE ID = '" . $ID . "';");

        if ($response === false) {

            return 1;
        }

        return 0;

    }

    function UpdateResponseByID($ID)
    {
        $response = $this->db->query("UPDATE LOW_PRIORITY IGNORE Answers SET CONTENT='" . $ID[0] . "', VISABLE='" . $ID[1] . "';");

        if ($response === false) {
            return 1;
        }
        return 0;
    }


}