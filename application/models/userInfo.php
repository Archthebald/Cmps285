<?php

class Userinfo extends CI_Model
{

    public $Email;
    public $ID;
    public $firstName;
    public $lastName;


    public function __construct($first, $second)
    {
        $this->load->database();

    }

    public function getUserData($userID)
    {
        $query = $this->db->query("SELECT ID, EMAIL, FIRST_NAME, LAST_NAME,ZIP,LNG, LAT, ZOOM FROM Users WHERE ID = '" . $userID . "';");

        $userinfo = $query->row_array();

        return $userinfo;
    }

    public function getUserLocation($userID)
    {
        $query = $this->db->query("SELECT ZIP,LNG, LAT FROM Users WHERE ID = '" . $userID . "';");

        $userLocation = $query->row_array();

        return $userLocation;
    }

    public function getUserPosts($userID)
    {
        $query = $this->db->query("");

    }

    public function updateUserEmail($email, $id)
    {

        $data = array(
            'EMAIL' => $email,
        );

        $this->db->where('ID', $id);
        $this->db->update('Users', $data);

    }

    public function updateUserFirstName($fname, $id)
    {
        $data = array(
            'FIRST_NAME' => $fname,
        );

        $this->db->where('ID', $id);
        $this->db->update('Users', $data);
    }

    public function updateUserLng($lng, $id)
    {

        $data = array(
            'LNG' => $lng,
        );

        $this->db->where('ID', $id);
        $this->db->update('Users', $data);

    }

    public function updateUserLat($lat, $id)
    {

        $data = array(
            'LAT' => $lat,
        );

        $this->db->where('ID', $id);
        $this->db->update('Users', $data);

    }
    public function updateUserLastName($lname, $id)
    {
        $data = array(
            'LAST_NAME' => $lname,
        );

        $this->db->where('ID', $id);
        $this->db->update('Users', $data);
    }

    public function updateUserZip($zip, $id)
    {
        $data = array(
            'ZIP' => $zip,
        );

        $this->db->where('ID', $id);
        $this->db->update('Users', $data);

    }

    public function getUserAnswers($userID)
    {

    }
}
