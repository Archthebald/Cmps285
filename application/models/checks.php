<?php

class checks extends CI_Model
{

    //constructor to load as separate construct each call then die.
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function ValidCreate($infoUser)
    {
        if (!(strpos($infoUser['email'], "@"))) {
            return false;
        } elseif (!(strpos($infoUser['email'], ".com")) || (strpos($infoUser['email'], ".net")) || (strpos($infoUser['email'], ".edu"))) {
            return false;
        } elseif (!($infoUser['firstname'] != '')) {
            return false;
        } elseif (!($infoUser['lastname'] != '')) {
            return false;
        } elseif (!($infoUser['password'] != '')) {
            return false;
        } else return true;

    }

    function EmailFree($email)
    {
        $query = $this->db->query("select EMAIL from Users where Users.EMAIL ='" . $email . "'limit 1;");

        $row = $query->row_array();

        if (isset($row)) {
            $check['email'] = $row['EMAIL'];

            if ($check['email'] == $email) {
                return false;
            } elseif ($check['email'] == '') {
                return false;
            } else return false;
        } else return true;
    }

    function PasswordHashCheck($passWord)
    {

    }

    function UserID($userEmail)
    {
        //user id

        $query = $this->db->query("SELECT ID FROM Users WHERE EMAIL = '" . $userEmail . "';");

        $row = $query->row_array();

        if (isset($row)) {
            $return['ID'] = $row['ID'];
        }

        return $return['ID'];

    }

    function UserInfo($UserID)
    {
        $query = $this->db->Query("SELECT ID, EMAIL, FIRST_NAME, LAST_NAME,ZIP,LNG, LAT, ZOOM FROM Users WHERE ID = '" . $UserID . "';");

        $UserInfo = $query->row_array();


        if (isset($row)) {
            $UserInfo['ID'] = $row['ID'];
            $UserInfo['EMAIL'] = $row['EMAIL'];
            $UserInfo['FIRST_NAME'] = $row['FIRST_NAME'];
            $UserInfo['LAST_NAME'] = $row['LAST_NAME'];
            $UserInfo['ZIP'] = $row['ZIP'];
            $UserInfo['LNG'] = $row['LNG'];
            $UserInfo['LAT'] = $row['LAT'];
            $UserInfo['ZOOM'] = $row['ZOOM'];
        }
        return $UserInfo;
    }

    function CreateUser($email, $password, $firstname, $lastname, $salt)
    {
        $this->db->query("INSERT INTO Users (EMAIL,PASSWORD, FIRST_NAME, LAST_NAME, SALT) VALUES ('" . $email . "', '" . $password . "', '" . $firstname . "','" . $lastname . "','" . $salt . "');");

    }
}
