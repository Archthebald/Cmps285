<?php if (!defined('BASEPATH')) exit('No direct script access
allowed');

class UserLogin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("login");
        $this->load->model("userinfo");
        $this->load->model("checks");
        $this->load->helper('cookie');
        $this->load->library('session');

    }

    function logincheck()
    {
        $LoginInfo = $_POST;

        $DbResult = $this->login->GetLoginInfo($LoginInfo['email']);


        if (!$DbResult) {
            echo 1;
        } else {
            if (password_verify($LoginInfo['password'], $DbResult['password'])) {

                echo 0;

                $UID = $this->checks->UserID($LoginInfo['email']);

                $Cookie_Info = $this->checks->UserInfo($UID);

                $this->session;

                //The $_SESSION[]; has the following information and Indexes
                /*
                $UserInfo['ID'] = $row['ID'];
                $UserInfo['EMAIL'] = $row['EMAIL'];
                $UserInfo['FIRST_NAME'] = $row['FIRST_NAME'];
                $UserInfo['LAST_NAME'] = $row['LAST_NAME'];
                $UserInfo['ZIP'] = $row['ZIP'];
                $UserInfo['LNG'] = $row['LNG'];
                $UserInfo['LAT'] = $row['LAT'];
                $UserInfo['ZOOM'] = $row['ZOOM'];
                */

                $this->session->set_userdata($Cookie_Info);


            } else echo 1;
        }
    }

    function UserLogout()
    {
        $this->session->sess_destroy();
    }


}





