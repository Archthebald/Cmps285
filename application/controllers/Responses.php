<?php

class Responses extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('checks');
        $this->load->model('responseinformation');
        $this->load->model('postinformation');

    }

    function PostComent()
    {
        $Insert = array(
            'AUTHOR' => $_POST['1'],
            'PARENT_POST' => $_POST['2'],
            'CONTENT' => $_POST['3'],
            'PENNAME' => $_POST['4']
        );
        $test = str_replace(' ', '', $Insert['CONTENT']);

        IF ($test == "") {
            echo 1;
        } else {
            $this->responseinformation->CreateResopnseByParent($Insert);
        }
    }

    function GetResponsesByParent()
    {
        $PID = $_POST['PID'];

        $return = $this->responseinformation->GetResponsesByParentModel($PID);


        if ($return == 1) {
            echo "1";
        } else {
            echo json_encode($return);
        }

    }

    function GetParentByAuthorID()
    {

        $UID = $_POST['UID'];

        $ParentObj = $this->responseinformation->GetParentByAuthorModel($UID);

        $encode = json_encode($ParentObj);

        echo $encode;


    }
}