<?php

class Process extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('checks');
        $this->load->model('login');
        $this->load->helper('cookie');
        $this->load->library('session');
        $this->load->model('userinfo');
    }

    public function validate()
    {
        $infoUser = $_POST;

        $salt = base64_encode(openssl_random_pseudo_bytes(10));

        $infoUser['password'] = password_hash($infoUser['password'], PASSWORD_DEFAULT);

        if ($this->checks->ValidCreate($infoUser)) {

            if ($this->checks->EmailFree($infoUser['email'])) {

                $this->checks->CreateUser($infoUser['email'], $infoUser['password'], $infoUser['firstname'], $infoUser['lastname'], $salt);


                $UID = $this->checks->UserID($infoUser['email']);

                $Cookie_Info = $this->checks->UserInfo($UID);

                $this->session;

                $this->session->set_userdata($Cookie_Info);

                echo 0;
            } elseif (!$this->checks->EmailFree($infoUser['email'])) {
                echo 1;
            } else return (1);
        } else return (1);
    }

    public function testAjax($data)
    {
        echo json_encode($data);
    }

    public function SubmitAccountChanges()
    {
        $acountinfo = $_POST;

        if ($acountinfo['first_name'] != "") {
            $this->userinfo->updateUserFirstName($acountinfo['first_name'], $acountinfo['id']);
        }
        if ($acountinfo['last_name'] != "") {
            $this->userinfo->updateUserLastName($acountinfo['last_name'], $acountinfo['id']);
        }
        if ($acountinfo['zip'] != "") {
            $this->userinfo->updateUserZip($acountinfo['zip'], $acountinfo['id']);
        }
        if ($acountinfo['email'] != "") {
            $this->userinfo->updateUserEmail($acountinfo['email'], $acountinfo['id']);
        }
        if ($acountinfo['lng'] != "") {
            $this->userinfo->updateUserLng($acountinfo['lng'], $acountinfo['id']);
        }
        if ($acountinfo['lat'] != "") {
            $this->userinfo->updateUserLat($acountinfo['lat'], $acountinfo['id']);
        } else {
            echo 1;
        }

        $Cookie_Info = $this->checks->UserInfo($acountinfo['id']);

        $this->session;

        $this->session->set_userdata($Cookie_Info);

    }
}
