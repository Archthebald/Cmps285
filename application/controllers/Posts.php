<?php

class Posts extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('checks');
        $this->load->model('postinformation');
        $this->load->model('login');

    }

    function createPost()
    {
        $newpost = $_POST;

        $this->postinformation->CreatePost($newpost);

    }

    function getPostByUser()
    {
        $UID = $_POST['UID'];

        $raw = $this->postinformation->GetPostByUser($UID);

        $encode = json_encode($raw);

        echo $encode;
    }

    function getPostByUserXML()
    {

        $UID = $_POST;

        $this->load->dbutil();

        $query = $this->postinformation->GetPostByUser($UID['UID']);

        $config = array(
            'root' => 'root',
            'attribute' => 'attribute',
            'element' => 'element',
            'newline' => "\n",
            'tab' => "\t"
        );

        $xml = $this->dbutil->xml_from_result($query, $config);

        $this->output->set_content_type('xml');

        $this->output->set_output($xml);

    }

    function getPostByID()
    {

        $id = $_POST['id'];

        $result = $this->postinformation->Postbyid($id);

        echo json_encode($result);

    }

    function getAllPostsXML()
    {

        $this->load->dbutil();

        $query = $this->postinformation->allPostInfo();

        $config = array(
            'root' => 'root',
            'attribute' => 'attribute',
            'element' => 'element',
            'newline' => "\n",
            'tab' => "\t"
        );

        $xml = $this->dbutil->xml_from_result($query, $config);

        $this->output->set_content_type('xml');

        $this->output->set_output($xml);

    }

    function RemovePostByID()
    {
        $PID = $_POST['pid'];
        try {
            $this->postinformation->RemovePostByID($PID);
        } catch (mysqli_sql_exception $a) {
            ECHO $a;
        }
        echo 1;
    }

    function getAllPostZip($zip)
    {

    }

}