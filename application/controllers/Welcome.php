<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    public function index()
    {
        $config['sess_save_path'] = sys_get_temp_dir();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->library('session');
        $this->load->view('Main_landing');
    }


}
